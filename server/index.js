const path = require("path");
const express = require("express");
const app = express(); // create express app
const { PORT = 3000 } = process.env

console.log('PORT', PORT)

// add middlewares
app.use(express.static(path.join(__dirname, "..", "build")));
app.use(express.static("public"));

app.use((req, res, next) => {
  res.sendFile(path.join(__dirname, "..", "build", "index.html"));
});

// start express server on port 8080
app.listen(PORT, () => {
  console.log(`server started on port http://localhost:${PORT}`);
});
