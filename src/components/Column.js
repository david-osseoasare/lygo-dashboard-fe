import React from 'react';
import { styled } from '@material-ui/styles'
import Grid from '@material-ui/core/Grid'

const StyledGrid = styled(Grid)({
  height: '544px',
  width: '125px',
  padding: '0 !important',
  margin: 'auto',
})

function Column({children}) {
  return (
    <StyledGrid item>
      {children}
    </StyledGrid>
  );
}

export default Column;
