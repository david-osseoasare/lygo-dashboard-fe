import React from 'react';
import { styled } from '@material-ui/styles'

const StyledDiv = styled('div')({
  height: '30px',
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  fontSize: '12px',
})

function ColumnHeader({day="", date=""}) {
  return (
    <StyledDiv item>
      <div>{day}</div>
      <div>{date}</div>
    </StyledDiv>
  );
}

export default ColumnHeader;
