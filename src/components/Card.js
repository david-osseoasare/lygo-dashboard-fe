import React from 'react';
import { styled } from '@material-ui/styles'
import Paper from '@material-ui/core/Paper'

const StyledPaper = styled(Paper)({
  height: '58px',
  padding: '3px 9px !important',
  marginBottom: '5px',
  backgroundColor: '#7F7F7F',
  marginRight: '5px',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  alignItems: 'flex-end',
})

function Card({ children, isTransparent=false }) {
  const backgroundcolor = isTransparent ? 'transparent' : '#7F7F7F'

  return (
    <StyledPaper item elevation={0} style={{backgroundColor: backgroundcolor}}>
      {children}
    </StyledPaper>
  );
}

export default Card;
