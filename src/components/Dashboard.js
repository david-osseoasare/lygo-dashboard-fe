import React from 'react';
import Column from './Column';
import Card from './Card';
import ColumnHeader from './ColumnHeader';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

function Dashboard({salesdata}) {

  return (
    <Container styles={{overflow: 'hidden',}}>
      <div style={{width: '1035px'}}>
        <Grid container spacing={1}>
          <Column>
            <ColumnHeader date="Sales" />
            <Card isTransparent>
              <p>Forecast</p>
              <p>Actual</p>
              <p>Accuracy</p>
            </Card>
            <ColumnHeader date="Hours" />
            <Card isTransparent>
              <p>Forecast hours</p>
              <p>Actual shifts</p>
              <p>Actual vs rec. hours</p>
            </Card>
            <ColumnHeader date="Sales by type" />
            <Card isTransparent>
              <p>Food - forecast</p>
              <p>Actual</p>
              <p>Accuracy</p>
            </Card>
            <Card isTransparent>
              <p>Wet - forecast</p>
              <p>Actual</p>
              <p>Accuracy</p>
            </Card>
            <ColumnHeader date="Sales by channel" />
            <Card isTransparent>
              <p>Eat in - forecast</p>
              <p>Actual</p>
              <p>Accuracy</p>
            </Card>
            <Card isTransparent>
              <p>Take away - forecast</p>
              <p>Actual</p>
              <p>Accuracy</p>
            </Card>
          </Column>
          {salesdata.listOfDayForcast.map((data) =>
            <Column>
              <ColumnHeader day="" date={data.date}/>
              <Card>
                <p>£{data.forcastSales}</p>
                <p>{data.actualSales}</p>
                <p></p>
              </Card>
              <ColumnHeader day="" date=""/>
              <Card>
                <p>{data.forcastHours}</p>
                <p>{data.actualSales}</p>
                <p>{data.actualVsRecHours}</p>
              </Card>
              <ColumnHeader day="" date=""/>
              <Card>
                <p>£{data.foodForcast}</p>
                <p>{data.foodForcastActual}</p>
                <p>{data.foodAccuracy}</p>
              </Card>
              <Card>
                <p>£{data.wetFoodForcast}</p>
                <p>{data.wetFoodActual}</p>
                <p>{data.wetFoodAccuracy}</p>
              </Card>
              <ColumnHeader day="" date=""/>
              <Card>
                <p>£{data.eatInForcast}</p>
                <p>{data.eatInForcastActual}</p>
                <p>{data.eatInAccuracy}</p>
              </Card>
              <Card>
                <p>£{data.takeAwayForcast}</p>
                <p>{data.takeAwayActual}</p>
                <p>{data.takeAwayAccuracy}</p>
              </Card>
            </Column>
          )}
        </Grid>
      </div>
    </Container>
  )
}

export default Dashboard;
