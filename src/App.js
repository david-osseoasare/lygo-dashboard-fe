import React from "react";
import axios from "axios";
import Homepage from "./pages/Homepage";

import "./styles.css";

const salesdata = {
	"week": 47,
	"listOfDayForcast": [{
		"date": "29-11-2021",
		"dayOfWeek": 1,
		"uom": "£",
		"forcastSales": 2316,
		"actualSales": 0,
		"forcastHours": 58,
		"approxShiftsMin": 7,
		"approxShiftsMax": 10,
		"actualVsRecHours": "92.3%",
		"foodForcast": 1190,
		"foodForcastActual": 1303,
		"foodAccuracy": "91.3%",
		"wetFoodForcast": 1125,
		"wetFoodActual": 1001,
		"wetFoodAccuracy": "87.6%",
    "eatInForcast": 1190,
		"eatInForcastActual": 1303,
		"eatInAccuracy": "91.3%",
    "takeAwayForcast": 1125,
		"takeAwayActual": 1001,
		"takeAwayAccuracy": "87.6%",
		"projectionUpdate": {
			"previous": "21-11-2021",
			"current": "29-11-2021"
		}
	},
	{
		"date": "30-11-2021",
		"dayOfWeek": 2,
		"uom": "£",
		"forcastSales": 2316,
		"actualSales": 0,
		"forcastHours": 58,
		"approxShiftsMin": 7,
		"approxShiftsMax": 10,
		"actualVsRecHours": "92.3%",
		"foodForcast": 1190,
		"foodForcastActual": 1303,
		"foodAccuracy": "91.3%",
		"wetFoodForcast": 1125,
		"wetFoodActual": 1001,
		"wetFoodAccuracy": "87.6%",
    "eatInForcast": 1190,
		"eatInForcastActual": 1303,
		"eatInAccuracy": "91.3%",
    "takeAwayForcast": 1125,
		"takeAwayActual": 1001,
		"takeAwayAccuracy": "87.6%",
		"projectionUpdate": null
	},
	{
		"date": "01-12-2021",
		"dayOfWeek": 3,
		"uom": "£",
		"forcastSales": 2316,
		"actualSales": 0,
		"forcastHours": 58,
		"approxShiftsMin": 7,
		"approxShiftsMax": 10,
		"actualVsRecHours": "92.3%",
		"foodForcast": 1190,
		"foodForcastActual": 1303,
		"foodAccuracy": "91.3%",
		"wetFoodForcast": 1125,
		"wetFoodActual": 1001,
		"wetFoodAccuracy": "87.6%",
    "eatInForcast": 1190,
		"eatInForcastActual": 1303,
		"eatInAccuracy": "91.3%",
    "takeAwayForcast": 1125,
		"takeAwayActual": 1001,
		"takeAwayAccuracy": "87.6%",
		"projectionUpdate": null
	},
	{
		"date": "02-12-2021",
		"dayOfWeek": 4,
		"uom": "£",
		"forcastSales": 2316,
		"actualSales": 0,
		"forcastHours": 58,
		"approxShiftsMin": 7,
		"approxShiftsMax": 10,
		"actualVsRecHours": "92.3%",
		"foodForcast": 1190,
		"foodForcastActual": 1303,
		"foodAccuracy": "91.3%",
		"wetFoodForcast": 1125,
		"wetFoodActual": 1001,
		"wetFoodAccuracy": "87.6%",
    "eatInForcast": 1190,
		"eatInForcastActual": 1303,
		"eatInAccuracy": "91.3%",
    "takeAwayForcast": 1125,
		"takeAwayActual": 1001,
		"takeAwayAccuracy": "87.6%",
		"projectionUpdate": null
	},
	{
		"date": "03-12-2021",
		"dayOfWeek": 5,
		"uom": "£",
		"forcastSales": 2316,
		"actualSales": 0,
		"forcastHours": 58,
		"approxShiftsMin": 7,
		"approxShiftsMax": 10,
		"actualVsRecHours": "92.3%",
		"foodForcast": 1190,
		"foodForcastActual": 1303,
		"foodAccuracy": "91.3%",
		"wetFoodForcast": 1125,
		"wetFoodActual": 1001,
		"wetFoodAccuracy": "87.6%",
    "eatInForcast": 1190,
		"eatInForcastActual": 1303,
		"eatInAccuracy": "91.3%",
    "takeAwayForcast": 1125,
		"takeAwayActual": 1001,
		"takeAwayAccuracy": "87.6%",
		"projectionUpdate": null
	},
	{
		"date": "04-12-2021",
		"dayOfWeek": 6,
		"uom": "£",
		"forcastSales": 2316,
		"actualSales": 0,
		"forcastHours": 58,
		"approxShiftsMin": 7,
		"approxShiftsMax": 10,
		"actualVsRecHours": "92.3%",
		"foodForcast": 1190,
		"foodForcastActual": 1303,
		"foodAccuracy": "91.3%",
		"wetFoodForcast": 1125,
		"wetFoodActual": 1001,
		"wetFoodAccuracy": "87.6%",
    "eatInForcast": 1190,
		"eatInForcastActual": 1303,
		"eatInAccuracy": "91.3%",
    "takeAwayForcast": 1125,
		"takeAwayActual": 1001,
		"takeAwayAccuracy": "87.6%",
		"projectionUpdate": null
	},
	{
		"date": "05-12-2021",
		"dayOfWeek": 7,
		"uom": "£",
		"forcastSales": 2316,
		"actualSales": 0,
		"forcastHours": 58,
		"approxShiftsMin": 7,
		"approxShiftsMax": 10,
		"actualVsRecHours": "92.3%",
		"foodForcast": 1190,
		"foodForcastActual": 1303,
		"foodAccuracy": "91.3%",
		"wetFoodForcast": 1125,
		"wetFoodActual": 1001,
		"wetFoodAccuracy": "87.6%",
    "eatInForcast": 1190,
		"eatInForcastActual": 1303,
		"eatInAccuracy": "91.3%",
    "takeAwayForcast": 1125,
		"takeAwayActual": 1001,
		"takeAwayAccuracy": "87.6%",
		"projectionUpdate": null
	}
  ]
}

export default class App extends React.Component {
  state = {
    users: [],
  };
  // componentDidMount() {
  //   axios.get("/users.json").then((response) => {
  //     this.setState({ users: response.data });
  //   });
  // }



  render() {
    const { users } = this.state;
    return (
			<Homepage salesdata={salesdata} />
    );
  }
}

//	moment().format('DD, MM'); - should return 29 Nov
