import React from 'react';
import { styled } from '@material-ui/styles';
import Dashboard from '../components/Dashboard'

const StyledDiv = styled('div')({
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
})

export default function homepage({ salesdata }) {

  return (
    <StyledDiv>
      <Dashboard salesdata={salesdata} />
    </StyledDiv>
  )
}
